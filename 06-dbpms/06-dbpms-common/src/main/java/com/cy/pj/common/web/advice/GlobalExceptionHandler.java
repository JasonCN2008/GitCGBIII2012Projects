package com.cy.pj.common.web.advice;

import com.cy.pj.common.pojo.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice  //@ControllerAdvice+@ResponseBody
public class GlobalExceptionHandler {
    @ExceptionHandler(ShiroException.class)
    public JsonResult doHandleShiroException(ShiroException e){
        //e.printStackTrace();
        log.error("exception:",e);
        JsonResult result=new JsonResult();
        result.setState(0);
        if(e instanceof UnknownAccountException){
            result.setMessage("用户名不存在");
        }else if(e instanceof LockedAccountException){
            result.setMessage("账户已被锁定");
        }else if(e instanceof IncorrectCredentialsException){
            result.setMessage("密码不正确");
        }else if(e instanceof AuthorizationException){
            result.setMessage("没有权限");
        }else{
            result.setMessage("认证或授权失败");
        }
        return result;
    }

    @ExceptionHandler(RuntimeException.class)
    public JsonResult doHandleRuntimeException(RuntimeException e){
        e.printStackTrace();
        log.error("exception{}",e.getMessage());
        return new JsonResult( e);
    }
}

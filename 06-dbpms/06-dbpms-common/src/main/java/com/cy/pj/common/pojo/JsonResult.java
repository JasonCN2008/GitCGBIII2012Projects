package com.cy.pj.common.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 服务端响应结果标准设计
 */
@Data
@NoArgsConstructor
public class JsonResult implements Serializable {
    private static final long serialVersionUID = 5110901796917551720L;
    /**状态码设计*/
    private Integer state=1; //1-OK,0-Error
    /**状态信息*/
    private String message="Ok";
    /**查询响应结果*/
    private Object data;

    public JsonResult(String message){
        this.message=message;
    }
    public JsonResult(Integer state,String message){
        this.state=state;
        this.message=message;
    }
    public JsonResult(Object data){
       this.data=data;
    }
    public JsonResult(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }
//    public JsonResult(ISelect select){
//        this.data= PageUtil.startPage().doSelectPageInfo(select);
//    }
    //.....

}

package com.cy.pj.common.util;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import javax.servlet.http.HttpServletRequest;

public class PageUtil {
    public static <E> Page<E> startPage(){
        //获取请求对象
        HttpServletRequest request=ServletUtil.getRequest();
        //获取当前页码值
        String pageCurrentStr=request.getParameter("pageCurrent");
        //获取页面大小(每页最多显示多少条记录)
        String pageSizeStr=request.getParameter("pageSize");
        //将获取的字符串转换为整数
        Integer pageCurrent=StringUtil.isEmpty(pageCurrentStr)?1:Integer.parseInt(pageCurrentStr);
        Integer pageSize=StringUtil.isEmpty(pageSizeStr)?5:Integer.parseInt(pageSizeStr);
        //启动分页查询
        return PageHelper.startPage(pageCurrent,pageSize);
    }
}

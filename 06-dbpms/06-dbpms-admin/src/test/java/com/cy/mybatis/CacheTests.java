package com.cy.mybatis;

import com.cy.pj.sys.pojo.SysRole;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CacheTests {

     @Autowired
     private SqlSessionFactory sessionFactory;

     @Test
     void testFirstLevelCache(){//SqlSession
         SqlSession session=sessionFactory.openSession();
         String statement="com.cy.pj.sys.dao.SysRoleDao.selectRoles";
         List<SysRole> list1=session.selectList(statement);
         List<SysRole> list2=session.selectList(statement);//来自SqlSession的缓存
         System.out.println(list1==list2);
         session.close();
     }
    @Test
    void testSecondLevelCache(){//SqlSession
        SqlSession session=sessionFactory.openSession();
        String statement="com.cy.pj.sys.dao.SysRoleDao.selectRoles";
        List<SysRole> list1=session.selectList(statement);
        session.close();//此时会将list集合放入到二级缓存中
        list1.clear();
        session=sessionFactory.openSession();
        List<SysRole> list2=session.selectList(statement);
        System.out.println(list1==list2);
        System.out.println(list2.size());
        session.close();
    }
}

package com.cy;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SpringBootTest
public class MD5Tests {
    @Test
    void testMD501(){
        String pwd="123456";
        //String salt= UUID.randomUUID().toString();
        String salt="738c91a5-66b0-4110-95d6-4ad50fd4ec20";
        System.out.println("salt="+salt);
        //对密码进行加密
        SimpleHash sh=new SimpleHash("MD5", pwd,salt);
        //获取加密结果
        pwd=sh.toHex();//0fdcbe6679de077d78d33edb2b3108ec
        System.out.println(pwd);
    }
    @Test
    void MD5Test02() throws NoSuchAlgorithmException {
        String pwd="123456";
        MessageDigest md=MessageDigest.getInstance("MD5");
        byte[] array=md.digest(pwd.getBytes());
        System.out.println(array.length);//16byte==128bit
        //为了便于展示和读写一般将128位的二进制数转换成32位16进制数
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<array.length;i++){
            String s=Integer.toHexString(array[i]&0xFF);
            if(s.length()==1){
                s="0"+s;
            }
            sb.append(s);
        }
        System.out.println(sb.toString());//e10adc3949ba59abbe56e057f20f883e
    }

    @Test
    void testGeneratorKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator= KeyGenerator.getInstance("AES");
        SecretKey secretKey = keyGenerator.generateKey();
        String key= Base64.encodeToString(secretKey.getEncoded());
        System.out.println(key);
    }

}

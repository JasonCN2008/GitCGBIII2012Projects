package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.pojo.SysRole;

import java.util.List;

public interface SysRoleService {
    /**
     * 基于条件查询角色相关信息
     * @param sysRole 封装查询条件(基于用户查询，基于创建时间等)
     * @return 查询到的角色信息
     */
    List<SysRole> findRoles(SysRole sysRole);

    /**
     * 基于id查询角色以及角色对应的菜单信息
     * 查询方案3种：
     * 1)数据层执行多表关联查询(sys_roles left join sys_role_menus)
     * 2)数据层执行多表嵌套查询(两个select)
     * 3)业务发起多次单表查询
     * 3.1)select * from sys_roles where id=#{id}
     * 3.2)select menu_id from sys_role_menus where role_id=#{id}
     * @param id
     * @return
     */
    SysRole findById(Integer id);

    /**
     * 新增角色以及角色对应的菜单关系数据
     * 1)insert into sys_roles () values ()
     * 2)insert into sys_role_menus () values ()
     * @param sysRole
     * @return
     */
    int saveRole(SysRole sysRole);

    /**
     * 更新角色以及角色对应的关系数据
     * 1)更新角色自身信息(update sys_roles set ....)
     * 2)更新角色和菜单关系数据
     * 2.1) 删除原有关系delete from sys_role_menus where role_id=#{roleId}
     * 2.2) 添加新的关系insert into sys_role_menus () values ()
     * @param sysRole
     * @return
     */
    int updateRole(SysRole sysRole);

    /**
     * 查询角色id，name信息
     * 对用户分配角色时，首先需要将角色查询出来，这个业务可以参考用户添加操作
     * @return
     */
    List<CheckBox> findCheckRoles();


}

package com.cy.pj.sys.service.impl;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 菜单业务对象
 */
@Transactional(readOnly = false,
               rollbackFor = Throwable.class,
               isolation = Isolation.READ_COMMITTED)
@Service
public class SysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuDao sysMenuDao;

    @Transactional(readOnly = true)
    @Override
    public List<SysMenu> findMenus() {
        //方式1 (重点)
        return sysMenuDao.selectMenus();
        //方法2 (拓展)
        //查找所有菜单信息：select * from sys_menus
        //List<SysMenu> menus=sysMenuDao.selectMenus();
        //在业务层对数据进行封装(一级菜单，二级菜单，三级菜单，.....),递归算法
        //menus.stream().filter().collect()
    }

    @Transactional(readOnly = true)
    @Override
    public List<Node> findMenuTreeNodes() {
        return sysMenuDao.selectMenuTreeNodes();

    }

    @Override
    public int saveMenu(SysMenu menu) {
        return sysMenuDao.insertMenu(menu);
    }

    @Transactional(readOnly = true)
    @Override
    public SysMenu findById(Integer id) {
        SysMenu menu=sysMenuDao.selectById(id);
        if(menu==null)throw new RuntimeException("没有找到对应记录");
        return menu;
    }

    @Override
    public int updateMenu(SysMenu menu) {
        return sysMenuDao.updateMenu(menu);
    }
}

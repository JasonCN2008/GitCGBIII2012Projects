package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 基于此dao操作用户表数据
 */
@Mapper
public interface SysUserDao {

    /**
     * 基于用户名查找用户对象
     * @param username
     * @return
     */
    @Select("select * from sys_users where username=#{username}")
    SysUser selectUserByUsername(String username);

    /**
     * 基于条件(例如用户名，部门名称，创建时间，状态等)查询用户以及用户对应的部门信息
     * @param sysUser (用户封装查询条件)
     * @return 查询到的用户记录
     * select .... from sys_users left join sys_depts on ...  where .....
     */

    List<SysUser> selectUsers(SysUser sysUser);

    /**
     * 基于id查询用户以及用户对应的部门，角色相关信息
     * @param id 用户id
     * @return 查询到的用户信息
     */
    SysUser selectById(Integer id);

    /**
     * 更新用户信息
     * @param sysUser
     * @return
     */
    int updateUser(SysUser sysUser);

    /**
     * 新增用户
     * @param sysUser
     * @return
     */
    int insertUser(SysUser sysUser);

    /**
     * 修改用户状态
     * @param id 用户id
     * @param valid 状态值
     * @param modifiedUser 修改用户(将来是系统登录用户)
     * @return 更新的行数
     */
    @Update("update sys_users set valid=#{valid},modifiedUser=#{modifiedUser},modifiedTime=now() where id=#{id}")
    int validById(Integer id,Integer valid,String modifiedUser);


}










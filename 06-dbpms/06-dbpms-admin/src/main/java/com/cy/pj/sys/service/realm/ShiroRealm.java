package com.cy.pj.sys.service.realm;

import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.dao.SysUserDao;
import com.cy.pj.sys.pojo.SysUser;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * 定义shiro realm对象，基于此对象获取用户认证和授权信息，
 * 假如将来你的项目只做认证，不做授权，则继承继承AuthenticatingRealm对象即可
 */

public class ShiroRealm extends /*AuthenticatingRealm*/ AuthorizingRealm {

    @Autowired
    private SysUserDao sysUserDao;

    /**获取并封装认证信息
     * @param authenticationToken 为封装了客户端认证信息的一个令牌对象
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authenticationToken) throws AuthenticationException {
        //1.获取客户端提交的用户名
        String username=((UsernamePasswordToken) authenticationToken).getUsername();
        //2.基于用户名查询用户信息并校验
        SysUser user=sysUserDao.selectUserByUsername(username);
        if(user==null)throw new UnknownAccountException();
        if(user.getValid()==0)throw new LockedAccountException();
        //3.封装用户信息
        ByteSource credentialsSalt=
                ByteSource.Util.bytes(user.getSalt());
                //new SimpleByteSource(user.getSalt());
        String realmName=getName();
        System.out.println("realmName="+realmName);
        SimpleAuthenticationInfo info=
                new SimpleAuthenticationInfo(user,//principal用户身份(基于业务设置)
                user.getPassword(),//hashedCredentials 已加密的凭证(密码)
                credentialsSalt,//credentialsSalt (做了编码处理的加密盐对象)
                realmName);//realmName
        return info;
    }
    /**重写此方法的目的是，底层对用户输入的登录密码进行加密，需要算法*/
    @Override
    public CredentialsMatcher getCredentialsMatcher() {
           HashedCredentialsMatcher credentialsMatcher=
                new HashedCredentialsMatcher("MD5");
           credentialsMatcher.setHashIterations(1);
        return credentialsMatcher;
    }
    //也可以在构造方法中通过调用set方法设置加密策略
//    public ShiroRealm(){
//        HashedCredentialsMatcher credentialsMatcher=
//                new HashedCredentialsMatcher("MD5");
//        credentialsMatcher.setHashIterations(1);
//        setCredentialsMatcher(credentialsMatcher);
//    }
    @Autowired
    private SysMenuDao sysMenuDao;
    /**获取并封装授权信息*/
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principalCollection) {
        System.out.println("==doGetAuthorizationInfo==");
        //获取登录用户
       SysUser user=(SysUser)principalCollection.getPrimaryPrincipal();
       //基于登录用户id，查询用户权限
       Set<String> permissionSet=
       sysMenuDao.selectUserPermissions(user.getId());
      // if(permissionSet==null||permissionSet.isEmpty())
        //   throw new AuthorizationException();
       //封装用户权限信息
       SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
       info.setStringPermissions(permissionSet);
       return info;
    }
}

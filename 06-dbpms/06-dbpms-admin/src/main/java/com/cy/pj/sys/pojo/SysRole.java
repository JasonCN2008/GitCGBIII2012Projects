package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 9082244336801748222L;
    /**角色 id*/
    private Integer id;
    /**角色名称*/
    private String name;
    /**菜单 id (基于角色id查询到的或者添加时传入的)*/
    private List<Integer> menuIds;
    /**备注*/
    private String remark;
    /**创建时间*/
    private Date createdTime;
    /**修改时间*/
    private Date modifiedTime;
    /**创建用户*/
    private String createdUser;
    /**修改用户*/
    private String modifiedUser;

}

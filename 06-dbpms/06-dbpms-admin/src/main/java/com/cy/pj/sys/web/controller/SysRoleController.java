package com.cy.pj.sys.web.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.common.util.PageUtil;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role/")
public class SysRoleController {
    //@Autowired
    private SysRoleService sysRoleService;
    public SysRoleController(){}
    @Autowired
    public void setSysRoleService(SysRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    //http://localhost/role/checkRoles
    @GetMapping("checkRoles")
    public JsonResult findCheckRoles(){
        return new JsonResult(sysRoleService.findCheckRoles());
    }

    @PutMapping
    public JsonResult doUpdateRole(@RequestBody SysRole sysRole){
        sysRoleService.updateRole(sysRole);
        return new JsonResult("update ok");
    }
    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable  Integer id){
        return new JsonResult(sysRoleService.findById(id));
    }
    @PostMapping
    public JsonResult doSaveRole(@RequestBody SysRole sysRole){
        sysRoleService.saveRole(sysRole);
        return new JsonResult("save ok");
    }
    @GetMapping
    public JsonResult doFindRoles(SysRole sysRole/*, HttpServletRequest request*/){//pageCurrent,pageSize
        // return new JsonResult(sysRoleService.findRoles(sysRole));
        //        //获取当前页码值
//        String pageCurrentStr=request.getParameter("pageCurrent");
//        //获取页面大小(每页最多显示多少条记录)
//        String pageSizeStr=request.getParameter("pageSize");
//        //将获取的字符串转换为整数
//        Integer pageCurrent=pageCurrentStr==null||"".equals(pageCurrentStr)?1:Integer.parseInt(pageCurrentStr);
//        Integer pageSize=pageSizeStr==null||"".equals(pageSizeStr)?5:Integer.parseInt(pageSizeStr);
//        //启动分页查询
//        PageInfo<PageInfo> pageInfo=PageHelper.startPage(pageCurrent,pageSize).doSelectPageInfo(()->{
//            sysRoleService.findRoles(sysRole);
//        });
//        return new JsonResult(pageInfo);

        return new JsonResult(PageUtil.startPage().doSelectPageInfo(()->{
           sysRoleService.findRoles(sysRole);
        }));
        // return new JsonResult(()->sysRoleService.findRoles(sysRole));
    }
}

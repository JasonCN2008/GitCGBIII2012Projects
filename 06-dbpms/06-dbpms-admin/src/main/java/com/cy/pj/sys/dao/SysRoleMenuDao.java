package com.cy.pj.sys.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysRoleMenuDao {
    /**
     * 当修改角色信息时，要先删除原有角色和菜单关系数据，再添加新的关系数据。
     * 基于角色id删除角色和菜单关系数据
     * @param roleId 角色id
     * @return 删除的行数
     */
    @Delete("delete from sys_role_menus where role_id=#{roleId}")
    int deleteByRoleId(Integer roleId);
    /**
     * 将角色和菜单关系数据写入到数据库
     * @param roleId
     * @param menuIds
     * @return
     */
    int insertRoleMenus(Integer roleId, List<Integer> menuIds);
}

package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设计菜单pojo对象，基于此对象封装操作菜单信息
 * FAQ?
 * 1.为什么不使用map去存储？
 * 1)可读性比较差(打开map元素不知道其内部存了什么)
 * 2)值的类型不可控(值的类型Object)
 * 2.为什么实现Serializable接口？
 * 1)java规范中所有用于存储数据的对象都建议实现此接口。
 * 2)此接口的作用就是做标记，只有实现了接口规范的对象才可以序列化
 * 3)序列化的目的是将对象转换为字节存储到缓存,文件或进行网络传输
 * 3.为什么要添加serialVersionUID呢？
 * 1)为序列化和反序列实现过程中的一个版本标识。
 * 2)将字节反序列化时，需要和序列化时使用的id进行对比，相同则可反序列化。
 *
 * 4.你了解JSON序列化和反序列化？
 * 1)JSON序列化和反序列化是一种广义的序列化和反序列化实现。
 * 2)将一个对象转换为json格式的字符串称之为json序列化
 * 3)将一个json格式的字符串转换为pojo,map等对象称之为json反序列化。
 *
 * 5.你是如何分析遇到的问题的？4w1h
 * 1)w (when),何时出现的问题？(什么业务)
 * 2)w (what)，什么错误？(BindingExcepiton,NoSuchBeanDefinitionException,400,...)
 * 3)w (where), 哪里的错误?(cause by 从下向上找，没有cause by则看异常前5行)
 * 4)w (why),为什么会有这样的错呢?(不知道时就可以看笔记或讨论)
 * 5)h (how)，如何解决呢？
 */
@Data
public class SysMenu implements Serializable {
    private static final long serialVersionUID = 5531553051566760402L;
    /**菜单id(对应数据库表中的物理主键)*/
    private Integer id;
    /**菜单名称*/
    private String name;
    /**菜单url(对应要访问的资源)*/
    private String url;
    /**菜单类型(菜单，按钮)*/
    private Integer type;
    /**菜单序号，有需要可以对菜单进行排序*/
    private Integer sort;
    /**备注*/
    private String remark;
    /**上级菜单id*/
    private Integer parentId;
    /**上级菜单名称(菜单表中没有此字段)*/
    private String parentName;
    /**菜单授权标识(访问菜单时需要具备的权限)*/
    private String permission;
    /**记录创建时间*/
    private Date createdTime;
    /**记录最后一次的修改时间*/
    private Date modifiedTime;
    /**记录创建用户*/
    private String createdUser;
    /**记录修改用户*/
    private String modifiedUser;
    //需求：此记录只能由创建用户进行修改 (细粒度的权限控制)
}

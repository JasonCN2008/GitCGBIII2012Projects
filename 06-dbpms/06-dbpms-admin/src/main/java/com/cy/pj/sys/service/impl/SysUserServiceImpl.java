package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.util.ShiroUtil;
import com.cy.pj.sys.dao.SysUserDao;
import com.cy.pj.sys.dao.SysUserRoleDao;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.service.SysUserService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class SysUserServiceImpl implements SysUserService {
    //@Autowired
    private SysUserDao sysUserDao;
    //@Autowired
    private SysUserRoleDao sysUserRoleDao;
    //通过构造方法为属性赋值
    public SysUserServiceImpl(SysUserDao sysUserDao, SysUserRoleDao sysUserRoleDao) {
        this.sysUserDao = sysUserDao;
        this.sysUserRoleDao = sysUserRoleDao;
    }
   //@RequiresPermissions("sys:user:view")
    public List<SysUser> findUsers(SysUser sysUser){
        return sysUserDao.selectUsers(sysUser);
    }

    //在业务层启动分页拦截查询
//    @RequiresPermissions("sys:user:view")
//    @Override
//    public PageInfo<SysUser> findUsers(SysUser sysUser) {
//       return  PageUtil.startPage().doSelectPageInfo(()->{
//            sysUserDao.selectUsers(sysUser);
//        });
//        //return sysUserDao.selectUsers(sysUser);
//    }

    @Override
    public SysUser findById(Integer id) {
        //基于id查询用户以及用户对应部门信息
        SysUser sysUser=sysUserDao.selectById(id);//user,dept
        if(sysUser==null)
            throw new ServiceException("用户已不存在");
        //基于id查询用户对应的角色id
        List<Integer> roleIds = sysUserRoleDao.selectRoleIdsByUserId(id);//roles
        sysUser.setRoleIds(roleIds);
        return sysUser;
    }

    @Transactional
    @Override
    public int saveUser(SysUser sysUser) {
        //1.保存用户自身信息
        //对密码进行MD5加密(MD5是一种不可逆的加密算法，只能加密不能解密，相同内容加密结果也相同)
        String password=sysUser.getPassword();
        String salt= UUID.randomUUID().toString();//加密盐
        SimpleHash simpleHash=//借助shiro框架中的api对密码进行加密
        new SimpleHash("MD5",password,salt,1);
        password=simpleHash.toHex();//将加密结果转换为16禁止
        sysUser.setPassword(password);
        sysUser.setSalt(salt);
        int rows=sysUserDao.insertUser(sysUser);
        //保存用户角色关系数据
        sysUserRoleDao.insertUserRoles(sysUser.getId(),
                     sysUser.getRoleIds());
        return rows;
    }
    @Transactional
    @Override
    public int updateUser(SysUser sysUser) {
        //更新用户自身信息
        int rows=sysUserDao.updateUser(sysUser);
        if(rows==0)
            throw new ServiceException("用户可能已经不存在");
        //更新用户和角色关系数据
        sysUserRoleDao.deleteByUserId(sysUser.getId());
        sysUserRoleDao.insertUserRoles(sysUser.getId(), sysUser.getRoleIds());
        return rows;
    }

    @Transactional
    @Override
    public int validById(Integer id, Integer valid) {
        //获取用户权限Set<String> permissions
        //获取此方法上的权限表示sys:user:update
        //判定用户的权限中是否包含方法上的权限表示 flag= permissions.contains(sys:user:update)
        //if(flag){}else{}
        //String modifiedUser="admin";//后续是登录用户
        //获取登录用户
        //SysUser user=(SysUser)SecurityUtils.getSubject().getPrincipal();
        SysUser user= ShiroUtil.getUser();
        String modifiedUser=user.getUsername();
        System.out.println("modifiedUser="+modifiedUser);
        return sysUserDao.validById(id, valid, modifiedUser);
    }
}

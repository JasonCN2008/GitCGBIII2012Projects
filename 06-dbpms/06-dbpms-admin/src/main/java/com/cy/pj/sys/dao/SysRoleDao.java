package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysRoleDao {
    /**
     * 基于条件查询角色信息
     * @param sysRole 封装了查询条件
     * @return 查询到的角色列表信息
     */
    List<SysRole> selectRoles(SysRole sysRole);

    /**
     * 基于角色id查询角色信息
     * @param id 角色id
     * @return 查询到的角色对象
     */
    SysRole selectById(Integer id);

    /**
     * 新增角色信息 (对象持久化)
     * @param sysRole 封装了角色信息的对象
     * @return 新增的行数
     */
    int insertRole(SysRole sysRole);
    /**
     * 更新角色信息 (对象持久化)
     * @param sysRole 封装了角色信息的对象
     * @return 更新的行数
     */
    int updateRole(SysRole sysRole);

    /**
     * 查询角色id，名字然后一行记录封装为一个checkbox对象。
     * 后续会将查询到的数据应用到用户模块
     * @return
     */
    @Select("select id,name from sys_roles")
    List<CheckBox> selectCheckRoles();

}

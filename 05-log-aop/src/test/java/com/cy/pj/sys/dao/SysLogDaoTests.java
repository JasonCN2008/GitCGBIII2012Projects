package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysLog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class SysLogDaoTests {
    /**
     * 假如你的idea中@Autowired这个注解下有波浪线没有关系，
     * 因为在编译阶段SysLogDao接口的实现类确实还不存在。因为
     * 这个接口的实现类是在运行时自动创建的。
     */
    @Autowired
    private SysLogDao sysLogDao;

    @Test
    void testInsertLog(){
        SysLog sysLog=new SysLog();
        sysLog.setUsername("Jason");
        sysLog.setIp("192.168.11.19");
        sysLog.setOperation("删除公告");
        sysLog.setMethod("com.cy.pj.notice.service.impl.SysNoticeServiceImpl.deleteById");
        sysLog.setParams("[1,2,3,4]");
        sysLog.setStatus(1);
        sysLog.setTime(1000L);
        sysLog.setCreatedTime(new Date());
        sysLogDao.insertLog(sysLog);
        System.out.println("insert ok");
    }
    @Test
    void testSelectById(){
        SysLog log=sysLogDao.selectById(176L);
        System.out.println(log);//要想直接输出内容，需要重写SysLog的toString方法
    }
    @Test
    void testDeleteById(){
        sysLogDao.deleteById(172L,173L,174L);
        System.out.println("delete ok");
    }
    @Test
    void testSelectLogs() throws ParseException {
       SysLog sysLog=new SysLog();
       sysLog.setUsername("admin");
       sysLog.setOperation("查询");
       sysLog.setStatus(1);
       String str="2020-05-01";
       SimpleDateFormat sdf=
               new SimpleDateFormat("yyyy-MM-dd");
       sysLog.setCreatedTime(sdf.parse(str));
       List<SysLog> logs=sysLogDao.selectLogs(sysLog);
       for(SysLog log:logs){
           System.out.println(log);
       }
    }
}

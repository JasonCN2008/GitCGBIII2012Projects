package com.cy;

import com.cy.pj.notice.pojo.SysNotice;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@Slf4j
@SpringBootTest
public class LombokTests {
    //private static final Logger log= LoggerFactory.getLogger(LombokTests.class);
    @Test
    void testLog(){
     //  log.debug("test lombok log");
    }
    @Test
    void testNotice(){
        SysNotice n1=new SysNotice(1L,"title-C","1","cotent-C","0","remark-c",new Date(),new Date(),"admin","admin");
        log.info("notice {}",n1.toString());
    }
}

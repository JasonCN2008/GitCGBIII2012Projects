package com.cy;

import com.cy.pj.notice.pojo.SysNotice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JSONTests {

     @Test
     void testJsonObject() throws JsonProcessingException {
         SysNotice notice=new SysNotice();
         notice.setId(100L);
         notice.setTitle("Study Json");
         notice.setContent("Json .....");
         notice.setRemark("Json remark...");
         notice.setStatus("0");
         notice.setType("1");
         //基于jackson中ObjectMapper类的实例将Pojo转换为json格式字符串
         //将对象转换为json字符串时，pojo对象需要提供get方法
         String jsonString=new ObjectMapper().writeValueAsString(notice);
         System.out.println(jsonString);
         //将json格式的字符串转换为SysNotice类型的对象
         SysNotice notice02= new ObjectMapper().
                 readValue(jsonString, SysNotice.class);
         System.out.println(notice02);
     }
}

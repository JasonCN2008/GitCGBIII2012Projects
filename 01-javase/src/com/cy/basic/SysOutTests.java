package com.cy.basic;

import java.io.PrintStream;

public class SysOutTests {
    static void doMethod1(){}
    static int doMethod02(){
        return 100;
    }
    public static void main(String[] args) {
       // System.out.println(doMethod01());//error
        System.out.println(doMethod02());
        PrintStream ps=System.out;
        ps.println("hello");
    }
}

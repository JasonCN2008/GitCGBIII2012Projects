package com.cy.basic;

/**
 * 一个.java程序中可以有多个类的定义，
 * 但是只能有一个类使用public修饰，
 * 并且.java文件的名字要与public修饰
 * 的类名相同
 */
public class Welcome {
}
class A{}
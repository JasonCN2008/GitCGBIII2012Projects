package com.cy.basic;

public class VarParamTests {
//    static int sum(int a,int b){
//        return a+b;
//    }
//    static int sum(int a,int b,int c){
//        return a+b+c;
//    }
//    static int sum(int a,int b,int c,int d){
//        return a+b+c+d;
//    }
    //可变参数应用(为了优化数组方式的传值，同时简化参数类型相同，参数个数不同的一些重载方法的定义)
    static int sum(int... array){//可变参数可以理解为一个特殊的数组
        int sum=0;
       for(int i=0;i<array.length;i++){
            sum+=array[i];
       }
        return sum;
    }
    //可变参数只能应用于方法参数的最后一个参数
    static  void doPrint(String s1,int... a){}
    public static void main(String[] args) {
        sum(10,20);
        sum(10,20,30);
    }
}

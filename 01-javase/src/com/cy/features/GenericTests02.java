package com.cy.features;

import java.util.ArrayList;
import java.util.List;

public class GenericTests02 {
    public static void main(String[] args) {
       //List<Object> list1=new ArrayList<String>();//错误
        List<? extends Object> list1=new ArrayList<String>();
       // List<String> list2=new ArrayList<Object>();//错误
        List<? super String> list2=new ArrayList<Object>();
    }
}

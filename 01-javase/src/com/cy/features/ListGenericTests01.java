package com.cy.features;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ListGenericTests01 {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<String> list=new ArrayList<>();//Object[]
        list.add("A");
        list.add("B");
        //因泛型的约束，我们不能在编译时将100这个整数存入list集合。
        //list.add(100);
        //你能否有办法将100个整数写入到集合中(考虑借助反射实现)
        //1).获取反射应用的起点对象(Class)
        Class<?> cls= list.getClass();
        //2).获取ArrayList类中的add方法对象
        Method method=cls.getDeclaredMethod("add",Object.class);
        //3).反射调用add方法，将100添加到集合
        method.invoke(list,100);//调用list对象的add方法，传入实际参数100
        System.out.println(list);
    }
}

package com.cy.api.collection;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapTests {

    public static void main(String[] args) {
        //假设为cache
        LinkedHashMap<String,Integer> map=
                new LinkedHashMap(3,0.75f,true){//LRU
                //new LinkedHashMap(3){//fifo
                /**每次执行put操作都会调用此方法*/
                @Override
                protected boolean removeEldestEntry(Map.Entry eldest) {
                   return size()>3;
                }
        };
        map.put("A", 100);
        map.put("B", 200);
        map.put("C", 300);
        map.get("A");
        map.put("D", 400);
        map.put("E", 500);
        System.out.println(map);
    }
}

package com.cy.api;

import java.util.concurrent.TimeUnit;

public class ThreadTests02 {
    static String content;
    public static void main(String[] args) throws InterruptedException {
        new Thread(){
            @Override
            public void run() {
                content="helloworld";
            }
        }.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println(content.toUpperCase());
    }
}

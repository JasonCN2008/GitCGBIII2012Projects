package com.cy.api;

public class ThreadTests01 {
    public static void main(String[] args) {
        new Thread(){
            @Override
            public void run() {
                System.out.println("thread00 is running");
            }
        }.start();
        new Thread(new Runnable() {//构建线程对象直接为线程分配一个任务，target (任务)
            @Override
            public void run() {
                System.out.println("thread 01 is running");
            }
        }).start();
        new Thread(new Runnable() {//target
            @Override
            public void run() {
                System.out.println("thread-02 is running");
            }
        },"thread-01").start();
        //lambda表达式应用，当接口内部只有一个抽象方法时，
        //可以通过lambda的方式简化构建匿名内部类对象的过程，
        //例如，(参数列表)->{}
        new Thread(()->{
            System.out.println("thread 02 is running");
        }).start();
        //当代码块中只有一条语句时，可以去掉{}
        new Thread(()->System.out.println("thread 03 running")).start();
    }
}

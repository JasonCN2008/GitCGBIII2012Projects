package com.cy.api.ref;

import java.lang.ref.WeakReference;

class Point{
    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize()");
    }
}
//-XX:+PrintGC
public class RefTests {
    public static void main(String[] args) {
        //1.强引用
        // Point p1=new Point();//强引用(此引用引用的对象即使内存不足也不会被GC)
        // p1=null;

        //HashMap<String,Object> cache=new HashMap<>();
        //cache.put("p1", new Point());//强引用
        //cache.clear();

       //2.软引用(允许在内存不足时清除软引用引用的对象)
//      SoftReference<Point> p2=new SoftReference<>(new Point());
//      System.out.println(p2.get());//使用

//       HashMap<String,Object> cache=new HashMap<>();
//       cache.put("p1", new SoftReference<>(new Point()));

        //3.弱引用(在GC触发时，运行清除弱引用引用的对象)
        WeakReference<Point> p3=new WeakReference<>(new Point());
        System.out.println(p3.get());//使用
        //手动GC
        System.gc();
        /*List<byte[]> list=new ArrayList<>();
        for(int i=0;i<1000;i++){
            list.add(new byte[1024*1024]);
        }*/
    }
}

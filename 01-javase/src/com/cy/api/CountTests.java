package com.cy.api;
class Counter{

    volatile int count;
    Counter(int count){
        this.count=count;
    }
    synchronized int getAndIncrement(){
         return count++;
    }
}

public class CountTests {

    public static void main(String[] args) throws InterruptedException {
          Counter counter=new Counter(0);
          for(int i=0;i<10;i++){
              new Thread(){
                  @Override
                  public void run() {
                     for(int i=0;i<1000;i++){
                         counter.getAndIncrement();
                     }
                  }
              }.start();
          }
          Thread.sleep(5000);
          System.out.println(counter.count);
    }
}

package com.cy.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ListTests {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(100);//list.add(Integer.valueOf(100))//集合中放的是什么？100的地址
        list.add(100);
        System.out.println(list.size());
        //迭代list集合
//      for(Integer a:list){
//        System.out.println(a);
//      }
        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });
        //lambda 表达式应用
        list.forEach((integer)->System.out.println(integer));
    }
}

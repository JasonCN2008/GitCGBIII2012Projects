package com.cy.api.exception;

public class ThrowTests {
    static void doMethod01(){
        //RuntimeException 为JDK中的运行时异常，也称之为编译阶段不检测的异常
        //抛出的所有runtimeexception或其子类异常，方法声明上都不需要抛出
        throw new RuntimeException("doMethod01 exception");
    }
    static void doMethod02() throws Exception{
        //Exception 为JDK中的编译时检查异常
        //当我们在方法内部抛出检查异常时，需要在方法声明上也要抛出同样或父类异常
        throw new Exception("doMethod02 Exception");
    }
    public static void main(String[] args) {
       //doMethod01();
       //doMethod02();//调用此方法时，要么try{}catch(){},要么main方法也抛出同样异常
    }
}

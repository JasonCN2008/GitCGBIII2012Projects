package com.cy.api;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicLongTests {
    public static void main(String[] args) {
        //Jdk1.5推出的线程安全对象
        AtomicLong at=new AtomicLong(1);
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        //减法
        System.out.println(at.getAndDecrement());
    }
}

package com.cy.api.task;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTests {
    public static void main(String[] args) {
        //通过timer实现任务调度(单线程任务调度，多个任务需要顺序执行)
        Timer t=new Timer();
        t.schedule(new TimerTask() {//任务
            @Override
            public void run() {
                System.out.println(System.currentTimeMillis());
            }
        }, 1000,1000);
    }
}

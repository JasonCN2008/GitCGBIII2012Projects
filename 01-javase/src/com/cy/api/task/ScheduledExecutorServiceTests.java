package com.cy.api.task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorServiceTests {
    public static void main(String[] args) {
        ScheduledExecutorService ses=
                Executors.newScheduledThreadPool(2);
        ses.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println(System.currentTimeMillis());
            }
        }, 1, 1, TimeUnit.SECONDS);
    }
}

package com.cy.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface Entity{
    String value() default "";
}
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
@interface ID{
    String value() default "";
}
@Entity("t_user")
class User{
    @ID("uid")
    private Integer id;
}

public class AnnotationTests {
    public static void main(String[] args) throws NoSuchFieldException, ClassNotFoundException {

        //1.获取user类上的entity注解中的内容
       //1.1获取User类的字节码对象
        Class<?> targetCls=Class.forName("com.cy.api.User");
        //Class<?> targetCls=User.class;
       //1.2获取User类上的Entity注解
       Entity entity=targetCls.getAnnotation(Entity.class);
       //1.3获取Entity注解value属性值
       String entityValue=entity.value();
       //String entityValue=User.class.getAnnotation(Entity.class).value();
        System.out.println(entityValue);

       //2.获取User类中id属性上注解ID的value属性值
       //2.1获取User类中的id属性
       Field f=targetCls.getDeclaredField("id");
       //2.2获取id属性上ID注解
       ID id=f.getAnnotation(ID.class);
       //2.3获取id注解中value属性的值
       String idValue=id.value();
       System.out.println(idValue);
    }
}
package com.cy.oop;

import java.util.ArrayList;
import java.util.List;
class Point{
    /**
     * 检测对象是否要被回收了，对象再被回收之前会执行此方法
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize()");
    }
}
//JVM参数配置:
// 1)-XX:+PrintGC  输出基本GC信息
// 2)-XX:+PrintGCDetails 输出详细GC信息
// 3)-Xmx5m -Xms5m
public class GCTests01 {//垃圾回收test
    public static void main(String[] args) {
        Point p1=new Point();
        //p1=null;
        //1.手动启动GC
        //System.gc();
        //2.自动启动GC
        List<byte[]> list=new ArrayList<>();
        list.add(new byte[1024 * 1024]);
        list.add(new byte[1024 * 1024]);
        list.add(new byte[1024 * 1024]);
        list.add(new byte[1024 * 1024]);
//        for(int i=0;i<1000;i++) {
//            list.add(new byte[1024 * 1024]);
//        }
    }
}
